# HogeSchoolTaal DarkMode

Dark mode voor hogeschooltaal.
Met een kleine easter egg.

[Open Userscript](https://gitlab.com/broodroosterdev/hogeschooltaal-darkmode/raw/master/hogeschooltaal_darkmode.user.js)

## Hoe installeer ik de userscript?
Om een userscript te installeren in je browser heb je de Tampermonkey extensie nodig. Deze kun je [hier downloaden](https://tampermonkey.net). Nadat je de extensie hebt geinstalleerd kun je de userscript inladen door op Open Userscript te klikken.

## Disclaimer
Gebruik de extensie niet voor tentamens. Ik ben niet verantwoordelijk voor onvoldoendes of problemen met de tentamencommisie omdat de userscript draaide tijdens je tentamen. Weet dat je de extensie uit kan zetten door op het Tampermonkey icon te klikken en op Enabled te drukken.