// ==UserScript==
// @name         HogeSchoolTaal Dark Mode
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Darkmode voor hogeschooltaal, met kleine easter egg.
// @author       broodroosterdev
// @match        https://windesheim.htaal.nl/*
// @downloadURL   https://gitlab.com/broodroosterdev/hogeschooltaal-darkmode/raw/master/hogeschooltaal_darkmode.user.js
// @updateURL     https://gitlab.com/broodroosterdev/hogeschooltaal-darkmode/raw/master/hogeschooltaal_darkmode.user.js
// @require https://cdn.jsdelivr.net/gh/mathusummut/confetti.js/confetti.min.js
// @grant        none
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

    //https://sinister.ly/Thread-Tutorial-How-To-Edit-CSS-within-TamperMonkey-Edit-Sites-CSS
    function addGlobalStyle(css) {
        var head, style;
        head = document.getElementsByTagName('head')[0];
        if (!head) { return; }
        style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = css;
        head.appendChild(style);
    }
    addGlobalStyle(`
body, #header_kleur, .wrapper_code2, .wrapper_code3, #content2, #content3, .wrapper_code1 { background: black !important; }
p, li { color: darkgray !important; }
#links_kleur, #rechts_kleur { width: 60% !important; }
#vraagstelling { color: white !important; }
.meerk_antw { background: darkgray !important; }
`);
    document.onclick = function() {
        window.setTimeout(function () {
            if(document.getElementById('eind_beoordeling') != null && document.getElementById('eind_beoordeling').innerText.includes("HEEL GOED")){
                confetti.start();
            }}, 1000);
    };

})();
